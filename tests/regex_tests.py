import unittest

import slate 

from pdf_parser.main import *

class TestPDFParser(unittest.TestCase):
	def test_find_between(self):
		words = "area: babadogo date: 22-12-2017"
		first = "area:"
		last = "date"

		words_between = "babadogo"

		self.assertEquals(words_between,
			find_between(words, first, last))

	def test_get_regions(self):
		pdfs_directory =  os.path.join(os.getcwd(), "tests/TestPDF")

		pdf_file = open(os.path.join(pdfs_directory, "interruptions - 07.12.2017 .pdf"))

		doc = slate.PDF(pdf_file)

		regions = get_regions(doc)

		expected_regions = ["NAIROBI NORTH", "NAIROBI WEST", "WESTERN", "NAIROBI SOUTH",
		"NORTH EASTERN", "SOUTH NYANZA", "COAST", "MT. KENYA"]

		self.assertEqual(set(expected_regions), set(regions))


def main():
	unittest.main()

if __name__ == "__main__":
	main()
