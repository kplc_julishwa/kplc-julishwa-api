import re
import os

import requests
from bs4 import BeautifulSoup


def get_site_content(site_url):
	"""
		Gets the site content of the webpage specified in the url
		and returns it

		Args:
			- site_url: The URL of the webpage
		Returns:
			- content: Content of the webpage
	"""
	page = requests.get(site_url)

	if not page.status_code in [200, 201]:
		return None

	return page.content

def extract_anchor_links(content):
	"""
		Obtains all <a> links and returns them as a collection/iterable object

		Args:
			- content: Site content as a string
		Returns:
			- links: bs4.element.ResultSet object

	"""
	soup = BeautifulSoup(content, "html.parser")
	return soup.find_all("a")


def extract_pdf_link(content):
	"""
		Obtains the <a> link for the blackout PDF and returns them as a collection/iterable object

		Args:
			- content: Site content as a string
		Returns:
			- links: bs4.element.ResultSet object

	"""
	soup = BeautifulSoup(content, "html.parser")
	return soup.find("a", class_="docicon")


def fetch_blackout_page_links(links):
	for link in links:
		if "interruptions - " in link.string.__repr__().lower().strip():
			yield link


def fetch_blackout_pdfs_links(blackout_page_links):
	"""
		Gets and returns the liinks to the various PDFs containing the schedule
	"""
	for blackout_page_link in blackout_page_links:
		blackout_page_content = get_site_content(blackout_page_link.get("href"))
		pdf_link = extract_pdf_link(blackout_page_content)
		yield pdf_link.get("href")


def main():
	# url = "http://kplc.co.ke/category/view/50/power-interruptions"
	kplc_site_url = os.getenv("KPLC_SITE_URL", None)
	
	if kplc_site_url:
		print "Fetching data from {0}".format(kplc_site_url)
		content = get_site_content(kplc_site_url)
		links = extract_anchor_links(content)

		blackout_page_links = fetch_blackout_page_links(links)

		blackout_pdfs = fetch_blackout_pdfs_links(blackout_page_links)

		# Check if PDFs directory exists
		pdf_dir = "../PDFs"
		if not os.path.exists(pdf_dir):
			os.makedirs(pdf_dir)

		# Fll the PDFs directory with the blackout pdfs
		for pdf in blackout_pdfs:
			pdf_link = str(pdf)
			pdf = requests.get(pdf_link)

			with open(os.path.join(pdf_dir + pdf_link[(pdf_link.rfind("Inter")):], "wb")) as f:
				f.write(pdf.content)
	else:
		print "KPLC_SITE_URL ENVIRONMENT VARIABLE not set"


if __name__ == "__main__":
	main()
