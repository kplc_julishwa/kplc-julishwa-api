from web_scraper import *

from pdf_parser.main import main as parse_pdfs

def get_schedule():
    """
        Fetches all PDFS available in the KPLC blackout schedule web page
    """

    kplc_site_url = os.getenv("KPLC_SITE_URL", None)

    if kplc_site_url:
    	print "Fetching data from {0}".format(kplc_site_url)
    	content = get_site_content(kplc_site_url)
    	links = extract_anchor_links(content)

    	blackout_page_links = fetch_blackout_page_links(links)
    	blackout_pdfs = fetch_blackout_pdfs_links(blackout_page_links)

        print "PDFs:"

    	# Check if PDFs directory exists
        pdf_dir = os.path.join(os.getcwd(), "PDFs")

    	if not os.path.exists(pdf_dir):
    		os.makedirs(pdf_dir)

    	# Fill the PDFs directory with the blackout pdfs
    	for pdf in blackout_pdfs:
            pdf_link = str(pdf)
            print pdf_link
            pdf = requests.get(pdf_link)

            file_path = ""
            if (pdf_link.rfind("Inter")) >= 0:
                file_path = pdf_link[pdf_link.rfind("Inter"):]
            elif (pdf_link[(pdf_link.rfind("INTER")):]) >= 0:
                file_path = pdf_link[(pdf_link.rfind("INTER")):]
            else:
                file_path = "unknown"

            file_path = os.path.join("PDFs", file_path.lower())

            with open(file_path, "wb") as f:
                f.write(pdf.content)

    	print "Fetched data from KPLC website\n"

        parse_pdfs()
    else:
		print "KPLC_SITE_URL ENVIRONMENT VARIABLE not set\n"


if __name__ == "__main__":
    get_schedule()
