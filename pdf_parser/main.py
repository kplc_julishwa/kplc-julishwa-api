import sys
import os
import re
import datetime
from collections import namedtuple

import slate

from api.models import *
from api.firebase.main import *


class Blackout(object):

	def __init__(self, area, date, time, places):
		self.area = area
		self.date = date
		self.time = time
		self.places =  places

	@staticmethod
	def parse_blackout(blackout_info):
		if blackout_info:
			blackout_info = blackout_info.strip()

			blackouts = []

			areas = re.findall("AREA.{1}\s*(.*?)DATE", blackout_info, flags=re.IGNORECASE)
			if not areas:
				areas = re.findall("AREA.{1}\s*(.*?)DAEE", blackout_info, flags=re.IGNORECASE)
			dates = re.findall("DATE.{1}\s*(.*?)TIME", blackout_info, flags=re.IGNORECASE)
			if not dates:
				dates = re.findall("DAEE.{1}\s*(.*?)TIME", blackout_info, flags=re.IGNORECASE)
			times = re.findall("TIME.{1}\s*(.*?)P\.M\.", blackout_info, flags=re.IGNORECASE)
			places = re.findall("P\.M\.(\s*.*?)customers", blackout_info, flags=re.IGNORECASE)

			for n in range(len(places)):
				blackouts.append(Blackout(
						areas[n],
						dates[n],
						times[n],
						places[n]
					))

			return blackouts

	def __repr__(self):
		return "Blackout<area=%s, date=%s, time=%s>" % (self.area, self.date, self.time)


class County_Info(object):
	
	def __init__(self, name):
		self.name = name
		self.blackouts = []

	def __repr__(self):
		return "County<name=%s, blackouts=%s>" % (self.name, self.blackouts)	


"""
	Identifies all the regions in the PDF document
"""
def get_regions(doc):
	regions = re.findall(r'etc.\)([^(]*?)REGION', doc[0], flags=re.IGNORECASE)
	regions += (re.findall(r'customers\.(\s*.{4,25}?)REGION', doc.text(), flags=re.IGNORECASE))

	return [region.strip() for region in regions]

"""
	Locates and returns a substring found in between two substrings
"""
def find_between(words, first, last):
	try:
	    res = re.search(first + "(.*)" + last, words)
	    return res.group(1).strip()
	except Exception as e:
		return None

"""
	Locates and returns counties found within regions
"""
def get_counties(doc, regions):
	blackout_info = []
	
	for n in range(1, len(regions)):
		region = find_between(doc.text(), regions[n-1] + " REGION", regions[n])
		counties = re.findall(r'^\s*(.*?)\s*COUNTY', region, flags=re.IGNORECASE)
		counties += re.findall(r'customers\.(\s*.{3,25}?)COUNTY', region, flags=re.IGNORECASE)

		region_counties = [County_Info(county.strip()) for county in counties]

		# KPLC sometimes forgets to categorise areas according to counties
		if region_counties:
			blackout_info.append((regions[n-1], [County_Info(county.strip()) for county in counties]))

	# Process the last region
	last_region_search_results = re.findall(regions[len(regions) - 1] + " REGION" + "(.*?)" + "For further", doc.text())

	if not last_region_search_results:
		# "For further might not exist at the end of the document"
		last_region_search_results = re.findall(regions[len(regions) - 1] + " REGION" + "(.*?)$", doc.text())

	if last_region_search_results:
		last_region = last_region_search_results[0]
		counties = re.findall(r'^\s*(.*?)\s*COUNTY', last_region, flags=re.IGNORECASE)
		counties += re.findall(r'customers\.(\s*.{3,25}?)COUNTY', last_region, flags=re.IGNORECASE)

		blackout_info.append((regions[n], [County_Info(county.strip()) for county in counties]))

	return blackout_info


"""
	Adds area information
"""
def get_areas(doc, counties_info):
	for m, c_info in enumerate(counties_info):
		# print "region: %s, county_info: %s" % (c_info[0], c_info[1])
		if len(c_info[1]) == 1:
			if len(counties_info)-1 >= m+1:
				text_range = find_between(doc.text(), c_info[1][0].name + " COUNTY", counties_info[m+1][0] + " REGION")
				c_info[1][0].blackouts = Blackout.parse_blackout(text_range)
		else:
			for n in range(0, len(c_info[1])-1):
				# print "Getting text btn (%s) and (%s)" % (c_info[1][n].name + " COUNTY", c_info[1][n+1].name + "COUNTY")
				text_range = find_between(doc.text(), c_info[1][n].name + " COUNTY", c_info[1][n+1].name + " COUNTY")
				# print "Text Range: %s" % (text_range)
				c_info[1][n].blackouts = Blackout.parse_blackout(text_range)

			if (m+1) < len(counties_info):
				text_range = find_between(doc.text(),
					c_info[1][len(c_info[1])-1].name + " COUNTY",
					counties_info[m+1][0] + " REGION")
				c_info[1][len(c_info[1])-1].blackouts = Blackout.parse_blackout(text_range)

				# print "Text btn (%s) and (%s) is: " % (c_info[1][n].name + " COUNTY", c_info[1][n+1].name + "COUNTY",  text_range)

	
	text_range = find_between(doc.text(), counties_info[-1][1][-1].name + " COUNTY", " For further")

	if not text_range:
		# "For further might not exist at the end of the document"
		text_range_results = re.findall(counties_info[-1][1][-1].name.strip() + " COUNTY" + "(.*?)$", doc.text())
		if text_range_results:
			text_range = text_range_results[0]

	if text_range:
		counties_info[-1][1][-1].blackouts = Blackout.parse_blackout(text_range)

	return counties_info
			
def get_latest_pdf(pdfs_directory):
	file_dates = []
	for filename in os.listdir(pdfs_directory):
		if filename.endswith("pdf"):
			file_dates.append(datetime.datetime.strptime(filename[-15:-4].strip(), "%d.%m.%Y"))

	file_dates.sort(reverse=True)
	latest = file_dates[0]

	for filename in os.listdir(pdfs_directory):
		if latest.strftime("%d.%m.%Y") in filename:
			return filename
	return None


def main():
	pdfs_directory =  os.path.join(os.getcwd(), "PDFs")
	
	# latest_file = get_latest_pdf(pdfs_directory)

	# Drop schedule tables
	if eng.dialect.has_table(eng, Area.__tablename__):
		Area.__table__.drop()
	if eng.dialect.has_table(eng, County.__tablename__):
		County.__table__.drop()
	if eng.dialect.has_table(eng, Region.__tablename__):
		Region.__table__.drop()
	
	Base.metadata.create_all()

	pdfs = os.listdir(pdfs_directory)
	# Ensure they are parsed in a certain order
	pdfs.sort()
	
	for filename in pdfs:
		if filename.endswith("pdf"):
			latest_file = filename
			print "Parsing (%s)" % (latest_file,)

			pdf_file = open(os.path.join(pdfs_directory, latest_file))
			doc = slate.PDF(pdf_file)

			regions = get_regions(doc)


			# Collect all the counties in all regions as a dictionary
			counties_info = get_counties(doc, regions)

			blackout_info = get_areas(doc, counties_info)
			
			print "%s regions detected" % (len(blackout_info),)

			for region, counties in blackout_info:
				if not ses.query(Region).filter_by(name=region.strip().lower()).first():
					ses.add(Region(region.strip().lower()))
					ses.commit()

				for county in counties:
					if not County.get_county_id(county.name.strip().lower(), region.strip().lower()):
						ses.add(County(county.name.strip().lower(), region.strip().lower()))
						ses.commit()

					for blackout in county.blackouts:
						try:
							actual_date = datetime.datetime.strptime(
								blackout.date[-11:-1].strip(), "%d.%m.%Y")

							if not (ses.query(Area).filter_by(name=blackout.area.strip().lower(),
								date=actual_date)).first():
								ses.add(Area(
									name = blackout.area.strip().lower(),
									date = actual_date,
									time = blackout.time.strip().lower() + " P.M.",
									places = blackout.places.strip().lower(),
									county_name = county.name.strip().lower(),
									region_name = region.strip().lower()))
								ses.commit()
						except ValueError as e:
							print "Unprecedented text in a PDF(%s)" % (e)

	print "\n\n"+ "."*20 + " Finished parsing PDFs (OK)\n"


	# Identify areas that have a blackout today

	todays_areas = ses.query(Area).filter(
		Area.date==datetime.datetime.now().date()).all()


	for area in todays_areas:
		# List of users subscribed to alerts on the current area
		user_ids = []

		# Check to see if there are subscribers for a location with a
		# blackout today
		subscribers = ses.query(Subscription).filter(
			Subscription.area_id==area.id).all()
		
		# Send Firebase notification to subscribed user
		for subscriber in subscribers:
			user_ids.append(subscriber.firebase_id)

		if user_ids:
			pushNotificationToUsers(user_ids,
				"Blackout scheduled in %s" % (area.name),
				"Time: %s\nPlaces: %s\n" % (area.time, area.places)
				)


if __name__ == "__main__":
	main()
