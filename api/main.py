import os

from flask import Flask

from models import *
from blackouts.views import blackouts_bp
from subscriptions.views import subscriptions_bp


def configure_app(app):
	"""
		Sets the app configurations in the order of:
			- Check ENV VAR
			- Load configuration object
			- Load configuration file
		The order of precedence is thus bottom-up

		Args:
			- app: An instance of Flask
	"""
	env_config = os.getenv("KPLC_CONFIG", "dev")

	# Possible configurations
	configs = {
		"prod":"api.config.ProductionConfig",
		"dev":"api.config.DevelopmentConfig"
	}

	# Load default or env selected one
	app.config.from_object(configs[env_config])

	# Load from configuration file
	config_file_path = "kplc_blackout.conf"
	if os.path.exists(config_file_path):
		app.config.from_pyfile(config_file_path)


app = Flask(__name__)

configure_app(app)

# Blueprint registration
app.register_blueprint(blackouts_bp)
app.register_blueprint(subscriptions_bp)


@app.route("/")
def index():
	return "KPLC API is under development"


if __name__ == "__main__":
	app.run()

