import os

from flask import current_app as app
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
# import jwt

from sqlalchemy import create_engine, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import sessionmaker, relationship

eng = create_engine(os.environ.get("DATABASE_URL"))

Base = declarative_base()
 
class Region(Base):
    __tablename__ = "region"
 
    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __init__(self, name):
        self.name = name

    @staticmethod
    def get_region_id(region_name):
        query = ses.query(Region).filter_by(name=region_name)
        if query:
            return query.first().id
        return None


class County(Base):
    __tablename__ = "county"
 
    id = Column(Integer, primary_key=True)
    name = Column(String)
    region_id = Column(Integer, ForeignKey("region.id"))

    region = relationship("Region", back_populates="counties")

    def __init__(self, name, region_name):
        self.name = name
        self.region_id = Region.get_region_id(region_name)

    @staticmethod
    def get_county_id(county_name, region_name):
        reg_id = Region.get_region_id(region_name)
        query = ses.query(County).filter_by(name=county_name, region_id=reg_id)

        if query.first():
            return query.first().id
        return None

Region.counties = relationship("County", order_by=County.id, back_populates="region")

class Area(Base):
    __tablename__ = "area"
 
    id = Column(Integer, primary_key=True)
    name = Column(String)
    date = Column(DateTime)
    time = Column(String)
    places = Column(String)
    county_id = Column(Integer, ForeignKey("county.id"))

    county = relationship("County", back_populates="areas")

    def __init__(self, name, date, time, places, county_name, region_name):
        self.name = name
        self.date = date
        self.time = time
        self.places = places
        self.county_id = County.get_county_id(county_name, region_name)

    def __repr__(self):
        return ("Area<name=%s, date=%s, time=%s>" % (
            self.name, self.date, self.time)).encode("utf-8")

County.areas = relationship("Area", order_by=Area.id, back_populates="county")


class Subscription(Base):
    __tablename__ = "subscription"
 
    id = Column(Integer, primary_key=True)
    firebase_id = Column(String)
    region_id = Column(Integer)
    county_id = Column(Integer)
    area_id = Column(Integer)

    def __init__(self, firebase_id, region_id, county_id, area_id):
        self.firebase_id = firebase_id
        self.region_id = region_id
        self.county_id = county_id
        self.area_id = area_id

    def __repr__(self):
        return "Subscription<firebase_id=%s, region_id=%s, county_id=%s, area_id=%s>" % (
            self.firebase_id,
            self.region_id,
            self.county_id,
            self.area_id)


Base.metadata.bind = eng
        
Session = sessionmaker(bind=eng)
ses = Session()    
