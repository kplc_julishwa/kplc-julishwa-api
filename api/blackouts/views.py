import datetime

from flask import Blueprint, request, make_response, jsonify
from flask.views import MethodView
from sqlalchemy import and_


from api.models import *

# ADD ITEMS API

class BlackoutsAPI(MethodView):
	"""
		Blackouts resource interface
	"""
	def get(self):
		blackouts = self._serialize_blackouts()

		return jsonify(dict(msg=blackouts))


	def _serialize_blackouts(self):
		regions = ses.query(Region).all()
		counties = ses.query(County).all()


		ser_regions = []
		for region in regions:
			counties = ses.query(County).filter_by(region_id=region.id).all()
			ser_counties = []
			for county in counties:
				areas = ses.query(Area).filter(and_(
					Area.date>=datetime.datetime.now().date(),
					Area.county_id==county.id)
					).all()
				if areas:
					ser_counties.append(dict(
						name=county.name,
						areas = [dict(
									name = area.name,
									date = datetime.datetime.strftime(area.date, "%A %d.%m.%Y"),
									time = area.time,
									places = area.places)
								for area in areas]))

			if ser_counties:
				ser_regions.append(dict(name=region.name, counties=ser_counties))

		return ser_regions




# Create views from the APIs
blackouts_view = BlackoutsAPI.as_view("blackouts")


# Create a blueprint instance for all item routes
blackouts_bp = Blueprint("blackouts", __name__)


# Specify the routes in the blueprint
blackouts_bp.add_url_rule("/blackouts", view_func=blackouts_view)
