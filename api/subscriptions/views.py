from flask import Blueprint, request, make_response, jsonify
from flask.views import MethodView


from api.models import *

# ADD SUBSCRIPTIONS API

class SubscriptionsAPI(MethodView):
	"""
		Subscriptions resource interface
	"""
	def get(self):
		"""
			Fetches and returns all the possible areas a user can subscribe to
		"""
		regions = ses.query(Region).all()

		possible_subscriptions = []

		for region in regions:
			possible_subscriptions.append(
				dict(
					name = region.name,
					counties = [dict(
						name = county.name,
						areas = [dict(
							name = area_name
							) for area_name in list(set(
								[area.name for area in ses.query(Area)
						.filter_by(county_id=County.get_county_id(county.name, region.name)).all()])
							)]
						) for county in ses.query(County)
					.filter_by(region_id=Region.get_region_id(region.name)).all()]))

		return jsonify(dict(msg=possible_subscriptions))


	def post(self):
		subscription_details = request.get_json(force=True)

		if not SubscriptionsAPI.validate_subscription_request_fields(subscription_details):
			return make_response(jsonify(dict(
			msg="Required fields not sent")), 400)

		if not SubscriptionsAPI.validate_subscription_request_filled(subscription_details):
			return make_response(jsonify(dict(
			msg="Some fields are empty")), 400)

		firebase_id = subscription_details["firebase_id"].strip()
		region_name = subscription_details["region"].strip().lower()
		county_name = subscription_details["county"].strip().lower()
		area_name = subscription_details["area"].strip().lower()

		# Get the ids
		try:
			region_id = Region.get_region_id(region_name)
			county_id = County.get_county_id(county_name, region_name)
			area_id = ses.query(Area).filter(
				Area.name==area_name).filter(Area.county_id==county_id).first().id
		except AttributeError as e:
			return make_response(jsonify(
				dict(msg="No location fitting those details")), 404)

		# Check if subscrpition already exists
		if ses.query(Subscription).filter(
			Subscription.firebase_id==firebase_id).filter(
			Subscription.region_id==region_id).filter(
			Subscription.county_id==county_id).filter(
			Subscription.area_id==area_id).first():

			return make_response(jsonify(
				dict(msg="Subscription already exists")), 409)

		# Store subscription details in database
		subscription = Subscription(
			firebase_id,
			region_id,
			county_id,
			area_id)

		ses.add(subscription)
		ses.commit()

		return make_response(jsonify(dict(
			msg="Successful subscription")), 200)

	@staticmethod
	def validate_subscription_request_fields(subscription_request):
		return "firebase_id" in subscription_request and \
			"region" in subscription_request and \
			"county" in subscription_request and \
			"area" in subscription_request

	@staticmethod
	def validate_subscription_request_filled(subscription_request):
		return subscription_request["firebase_id"] and \
			subscription_request["region"] and \
			subscription_request["county"] and \
			subscription_request["area"]


	def _serialize_subscriptions(self):
		pass




# Create views from the APIs
subscriptions_view = SubscriptionsAPI.as_view("subscriptions")


# Create a blueprint instance for all item routes
subscriptions_bp = Blueprint("subscriptions", __name__)


# Specify the routes in the blueprint
subscriptions_bp.add_url_rule("/subscriptions", view_func=subscriptions_view)
