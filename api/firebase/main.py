#!/usr/bin/python
from pyfcm import FCMNotification
import os, sys


def pushNotificationToUsers(user_ids, message_title, message_body):
	# DOCS: https://github.com/olucurious/PyFCM
	# and https://firebase.google.com/docs/cloud-messaging/http-server-ref
	API_KEY="AAAAW4PtQgQ:APA91bHVaD642B9U1777CzGrJ0M0jBxue_leKdARWvDlYmcPeFE19fc6k4CX6ThDa6RAdu24yTaSvHkW0KkhthUzzTta0HoC6P2rB7VM6PwCTIabMuW9bWnye-wChGHZIDJ8KHlETGX2"

	# ===========
	# Silent Push
	# ===========

	push_service = FCMNotification(API_KEY)

	# ===========================
	# Push a notification Message
	# ===========================

	# Send to multiple devices by passing a list of ids.
	result = push_service.notify_multiple_devices(
		registration_ids=user_ids,
		message_title=message_title,
		message_body=message_body,
		sound="Default")

	return result
