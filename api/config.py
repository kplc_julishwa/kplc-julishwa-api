import os


class BaseConfig(object):
	SQLALCHEMY_DATABASE_URI = os.environ.get("KPLC_BLACKOUT_DATABASE_URL")
	SECRET_KEY = os.environ.get("KPLC_BLACKOUT_SECRET_KEY")

class DevelopmentConfig(BaseConfig):
	SQLALCHEMY_TRACK_MODIFICATIONS = True
	DEBUG = True

class ProductionConfig(BaseConfig):
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	DEBUG = False
